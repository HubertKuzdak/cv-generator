const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');


const PdfPrinter = require('pdfmake')
const fs = require('fs');

const path = require('path');
const session = require('express-session');

const errorHandler = require('errorhandler');

const pdfTemplate = require('./static/basic');

const puppeteer = require('puppeteer');



//Configure mongoose's promise to global promise
mongoose.promise = global.Promise;

//Configure isProduction variable
const isProduction = process.env.NODE_ENV === 'production';

const corsOptions = {
  origin: '*',
  optionsSuccessStatus: 200,
}

const app = express();

app.use(cors(corsOptions));
app.use(bodyParser.json());
app.use(express.static('static'));
app.use(session({ secret: 'passport-tutorial', cookie: { maxAge: 60000 }, resave: false, saveUninitialized: false }));


if (!isProduction) {
  app.use(errorHandler());
}

//Configure Mongoose
mongoose.connect(
  "mongodb://mo1175_cvgen:33P3tv8qD7vGzut9hhdW@85.194.242.93:27017/mo1175_cvgen",
  {
    useMongoClient: true
  }
);
mongoose.set('debug', true);

require('./models/User');
require('./config/passport');
app.use(require('./routes'));

//Error handlers & middlewares
// if (!isProduction) {
//   app.use((err, req, res) => {
//     // res.status(err.status || 500);

//     res.json({
//       errors: {
//         message: err.message,
//         error: err,
//       },
//     });
//   });
// }

// app.use((err, req, res) => {
//   res.status(err.status || 500);

//   res.json({
//     errors: {
//       message: err.message,
//       error: {},
//     },
//   });
// });

app.post('/generate-cv', function (req, res) {
  const { skills, hobbys, experience, education, languages } = req.body;

  const templateURL = req.protocol + '://' + req.get('host') + `/basic.html`;

  const cvTemplateHTML = pdfTemplate({ skills, hobbys, experience, education, languages });

  fs.writeFileSync('./static/basic.html', cvTemplateHTML);

  const createPdf = async () => {
    const browser = await puppeteer.launch({ ignoreHTTPSErrors: true });
    const page = await browser.newPage();
    const options = {
      path: './static/cv/basic.pdf',
      printBackground: true,
      format: 'A4',
    };

    await page.goto(templateURL, { waitUntil: 'networkidle2' });
    await page.emulateMedia('screen');
    await page.pdf(options);
    await browser.close();
  }

  createPdf();

  const fullUrl = req.protocol + '://' + req.get('host') + `/cv/basic.pdf`;

  res.json({
    url: fullUrl,
    status: 200
  });
});

const PORT = process.env.PORT || 5000;
app.listen(PORT);
