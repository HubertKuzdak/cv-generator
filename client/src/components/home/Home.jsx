import React, { Component } from 'react';
import './Home.scss';
import axios from 'axios';

const API_URL = 'http://localhost:5000';

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedThemeMode: 'own-theme',
            fileSelected: false,
            generatedCvTemplateLink: '',
            form: {
                data: {
                    fullname: 'Jan Kowalski',
                    adress: 'Cecilia Chapman 711-2880',
                    phone: '(541) 754-3010',
                    email: 'text@test.pl'
                },
                expierence: [
                    { value: '1' },
                    { value: '2' }
                ],
                skills: [
                    { value: '' }
                ],
                education: [
                    { value: '' }
                ],
                interests: [
                    { value: '' }
                ],
                languages: [
                    { value: '' }
                ]
            }
        }
    }

    handleCreateTheme = (event) => this.setState({ selectedThemeMode: event.target.id });

    handleInputChange = () => {
        this.setState({ fileSelected: true }, () => setTimeout(() => {
            this.setState({ selectedThemeMode: 'own-theme' })
        }, 1000))
    };

    handleAddNewField = (event) => {
        const type = event.target.id;
        const newValue = this.state.form[type].concat({ value: '' });

        this.setState(prevState => ({
            form: {
                ...prevState.form,
                [type]: newValue
            }
        }));
    }

    handleInputFormChange = (event) => {
        const { value } = event.target;
        const keyType = event.target.getAttribute('keyType');
        const keytypeindex = event.target.getAttribute('keytypeindex');
        const newValue = this.state.form[keyType];
        newValue[keytypeindex].value = value;

        this.setState(prevState => ({
            form: {
                ...prevState.form,
                [keyType]: newValue
            }
        }));
    }

    handleDataInputChange = (event) => {
        const type = event.target.id;
        const { value } = event.target;

        this.setState(prevState => ({
            form: {
                ...prevState.form,
                data: {
                    ...prevState.form.data,
                    [type]: value
                }
            }
        }));
    }


    handleSubmit = async () => {
        const { form } = this.state;
        console.log(form);

        try {
            const { data: { url } } = await axios.post(`${API_URL}/generate-cv`, form);

            this.setState({ generatedCvTemplateLink: url });
        } catch (error) {
            console.error(error);
        }
    }

    render() {
        const { selectedThemeMode, generatedCvTemplateLink, fileSelected,
            form: {
                data: {
                    fullname,
                    adress,
                    phone,
                    email,
                },
                expierence, skills, education, interests, languages } } = this.state;
        return (
            <div>
                {!selectedThemeMode &&
                    <div className="template-select">
                        <div>
                            <button className="btn btn-raised btn-primary" onClick={this.handleCreateTheme} id="own-theme">Stwórz własny szablon</button>
                        </div>
                        <div>
                            <button className="btn btn-outline-primary" onClick={this.handleCreateTheme} id="existing-theme">Wybierz istniejący szablon</button>
                        </div>
                    </div>
                }

                {selectedThemeMode === 'existing-theme' &&
                    <div className="selct-file">
                        <div className="card">
                            <div className="card-body">
                                <h5 className="card-title text-uppercase text-center">Wybierz swoje CV</h5>
                                {fileSelected && <p className="text-center lead text-info">Trwa ładowanie CV...</p>}
                                <div className="input-group">
                                    <div className="custom-file">
                                        <input type="file" className="custom-file-input" onChange={this.handleInputChange} />
                                    </div>
                                    <div className="input-group-append">
                                        <button className="btn btn-outline-secondary" type="button">Wybierz</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                }

                {selectedThemeMode === 'own-theme' &&
                    <div className="cv-form">
                        <h2 className="text-center">Twoje dane - nowy szablon cv</h2>
                        <div className="data-group">
                            <h3 className="data-group-title text-center">Dane osobowe</h3>
                            <div className="form-group">
                                <label htmlFor="name" className="bmd-label-floating">Imię i nazwisko</label>
                                <input type="text"
                                    className="form-control"
                                    id="fullname"
                                    defaultValue={fullname}
                                    onChange={this.handleDataInputChange} />
                            </div>
                            <div className="form-group">
                                <label htmlFor="adress" className="bmd-label-floating">Adress</label>
                                <input type="text"
                                    className="form-control"
                                    id="adress"
                                    defaultValue={adress}
                                    onChange={this.handleDataInputChange} />
                            </div>
                            <div className="form-group">
                                <label htmlFor="phone" className="bmd-label-floating">Numer telefonu</label>
                                <input type="text"
                                    className="form-control"
                                    id="phone"
                                    defaultValue={phone}
                                    onChange={this.handleDataInputChange} />
                            </div>
                            <div className="form-group">
                                <label htmlFor="email" className="bmd-label-floating">Email</label>
                                <input type="text"
                                    className="form-control"
                                    id="email"
                                    defaultValue={email}
                                    onChange={this.handleDataInputChange} />
                            </div>
                        </div>
                        <div className="data-group">
                            <h3 className="data-group-title text-center">Doświadczenie</h3>
                            {expierence.map((item, index) =>
                                <div key={index} className="form-group">
                                    <input type="text"
                                        className="form-control"
                                        defaultValue={item.value}
                                        onChange={this.handleInputFormChange}
                                        keytypeindex={index}
                                        keyType='expierence' />
                                </div>
                            )}
                            <button className="btn btn-primary" onClick={this.handleAddNewField} id="expierence">Dodaj nowe pole</button>
                        </div>
                        <div className="data-group">
                            <h3 className="data-group-title text-center">Umiejętności</h3>
                            {skills.map((item, index) =>
                                <div key={index} className="form-group">
                                    <input type="text"
                                        className="form-control"
                                        defaultValue={item.value}
                                        onChange={this.handleInputFormChange}
                                        keytypeindex={index}
                                        keyType='skills' />
                                </div>
                            )}
                            <button className="btn btn-primary" onClick={this.handleAddNewField} id="skills">Dodaj nowe pole</button>
                        </div>
                        <div className="data-group">
                            <h3 className="data-group-title text-center">Wykształcenie</h3>
                            {education.map((item, index) =>
                                <div key={index} className="form-group">
                                    <input type="text"
                                        className="form-control"
                                        defaultValue={item.value}
                                        onChange={this.handleInputFormChange}
                                        keytypeindex={index}
                                        keyType='education' />
                                </div>
                            )}
                            <button className="btn btn-primary" onClick={this.handleAddNewField} id="education">Dodaj nowe pole</button>
                        </div>
                        <div className="data-group">
                            <h3 className="data-group-title text-center">Zainteresowania</h3>
                            {interests.map((item, index) =>
                                <div key={index} className="form-group">
                                    <input type="text"
                                        className="form-control"
                                        defaultValue={item.value}
                                        onChange={this.handleInputFormChange}
                                        keytypeindex={index}
                                        keyType='interests' />
                                </div>
                            )}
                            <button className="btn btn-primary" onClick={this.handleAddNewField} id="interests">Dodaj nowe pole</button>
                        </div>
                        <div className="data-group">
                            <h3 className="data-group-title text-center">Języki</h3>
                            {languages.map((item, index) =>
                                <div key={index} className="form-group">
                                    <input type="text"
                                        className="form-control"
                                        defaultValue={item.value}
                                        onChange={this.handleInputFormChange}
                                        keytypeindex={index}
                                        keyType='languages' />
                                </div>
                            )}
                            <button className="btn btn-primary" onClick={this.handleAddNewField} id="languages">Dodaj nowe pole</button>
                        </div>
                        {generatedCvTemplateLink &&
                            <div className="data-group btn-container">
                                <h4 className="text-center text-uppercase">Toje Cv jest gotowe!</h4>
                                <a href={generatedCvTemplateLink} target="_blank" rel="noopener noreferrer" className="download-cv-link">
                                    <button className="btn btn-success btn-raised btn-generate-tempalte">Kliknij aby zobaczyć podgląd</button>
                                </a>
                            </div>
                        }
                        {!generatedCvTemplateLink &&
                            <div className="data-group btn-container">
                                <button className="btn btn-primary btn-raised btn-generate-tempalte" onClick={this.handleSubmit}>Wygeneruj CV</button>
                            </div>
                        }
                    </div>
                }
            </div>
        );
    }
}

export default Home;