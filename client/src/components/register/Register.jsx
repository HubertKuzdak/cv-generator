import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import { withRouter } from 'react-router-dom';
import 'react-notifications/lib/notifications.css';
import { NotificationContainer, NotificationManager } from 'react-notifications';

class Register extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            formErrors: { email: '', password: '' },
            emailValid: false,
            passwordValid: false,
            formValid: false
        }
    }

    validateField = (fieldName, value) => {
        let fieldValidationErrors = this.state.formErrors;
        let emailValid = this.state.emailValid;
        let passwordValid = this.state.passwordValid;

        switch (fieldName) {
            case 'email':
                emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
                fieldValidationErrors.email = emailValid ? '' : ' is invalid';
                break;
            case 'password':
                passwordValid = value.length >= 6;
                fieldValidationErrors.password = passwordValid ? '' : ' is too short';
                break;
            default:
                break;
        }
        this.setState({
            formErrors: fieldValidationErrors,
            emailValid: emailValid,
            passwordValid: passwordValid
        }, this.validateForm);
    }

    errorClass = (error) => (error.length === 0 ? '' : 'has-error');

    validateForm = () => {
        this.setState({ formValid: this.state.emailValid && this.state.passwordValid });
    }

    handleUserInput = (event) => {
        const id = event.target.id;
        const value = event.target.value;

        this.setState({ [id]: value }, () => { this.validateField(id, value) });
    }

    handleSubmit = async (e) => {
        e.preventDefault();
        try {
            const { email, password } = this.state;
            const user = Object.assign({}, { email }, { password });
            const { token } = await axios.post('http://localhost:5000/api/users/', { user });
            if (window) {
                window.localStorage.setItem('token', token);
            }
            NotificationManager.success('Za chwilę nastąpi przekierowanie do strony', 'Konto zostało utworzone');
            setTimeout(() => {
                this.props.history.push('/');
            }, 1500);
        } catch (error) {
            console.error(error);

        }
        // const response = axios.post('http://localhost:5000/api/users/');
    }

    render() {
        const { email, password } = this.state;

        return (
            <div className="form-wrapper">
                <NotificationContainer />
                <div className="container">
                    <div className="row">
                        <div className="col-sm-9 col-md-7 col-lg-5 mx-auto">
                            <div className="card card-signin my-5">
                                <div className="card-body">
                                    <h5 className="card-title text-center">Rejestracja</h5>
                                    <form className="form-signin">
                                        <div className="form-label-group">
                                            <input
                                                id="email"
                                                type="email"
                                                className="form-control"
                                                placeholder="Email"
                                                defaultValue={email}
                                                onChange={this.handleUserInput}
                                                required
                                                autoFocus
                                            />
                                            <label htmlFor="email">Email</label>
                                        </div>

                                        <div className={`form-label-group ${this.errorClass(this.state.formErrors.password)}`}>
                                            <input
                                                id="password"
                                                type="password"
                                                className="form-control"
                                                placeholder="Hasło"
                                                defaultValue={password}
                                                onChange={this.handleUserInput}
                                                required
                                            />
                                            <label htmlFor="password">Hasło</label>
                                        </div>
                                        <button
                                            className="btn btn-lg btn-primary btn-raised btn-block text-uppercase button-submit"
                                            disabled={!this.state.formValid}
                                            onClick={this.handleSubmit}
                                        >
                                            Wyślij
                                        </button>
                                        <hr className="my-4" />
                                        <Link to="/login"><button className="btn btn-lg btn-facebook btn-block text-uppercase"><i className="fab fa-facebook-f mr-2"></i> Logowanie</button></Link>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default withRouter(Register);