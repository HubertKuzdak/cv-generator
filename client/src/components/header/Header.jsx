import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';

class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showNavbar: false,
        }
    }

    componentDidMount() {
        const { pathname } = this.props.location;

        if (pathname === '/login' || pathname === '/register' || pathname === '/profil') {
            this.setState({ showNavbar: false })

        } else {
            this.setState({ showNavbar: true })
        }

    }

    render() {
        const { showNavbar } = this.state;

        if (showNavbar) {
            return (
                <nav className="navbar navbar-expand-lg navbar-light bg-light">
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarNav">
                        <ul className="navbar-nav">
                            <li className="nav-item active">
                                <a className="nav-link" href="/profil">Dashboard <span className="sr-only">(current)</span></a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="/login">Login</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="/register">Register</a>
                            </li>
                        </ul>
                    </div>
                </nav>
            )
        }

        return null;
    }
}

export default withRouter(Header);