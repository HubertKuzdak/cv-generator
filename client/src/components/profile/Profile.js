import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import './Profile.css'

class Profile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            activeNav: 'subscriptions'
        }
    }

    handleActiveNav = (event) => {
        const id = event.currentTarget.id;
        console.log(id);

        this.setState({ activeNav: id })
    }

    render() {
        const { activeNav } = this.state;

        return (
            <div className="d-flex">
                <div>
                    <ul className="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

                        <a className="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
                            <div className="sidebar-brand-icon rotate-n-15">
                                <i className="fas fa-laugh-wink"></i>
                            </div>
                            <div className="sidebar-brand-text mx-3">Profil</div>
                        </a>

                        <hr className="sidebar-divider my-0" />

                        <li className="nav-item active" onClick={this.handleActiveNav} id="subscriptions">
                            <div className="nav-link">
                                <span>Subskrypcje</span>
                            </div>
                        </li>
                        <li className="nav-item active">
                            <a href="/">
                                <div className="nav-link">
                                    <span>strona główna</span>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>
                <div className="p-4 w-100">
                    {activeNav === 'subscriptions' &&
                        <div clas>
                            <div className="container-fluid">

                                <div className="d-sm-flex align-items-center justify-content-between mb-4">
                                    <h1 className="h3 mb-0 text-gray-800">Subskrypcje</h1>
                                </div>

                                <div className="row">

                                    <div className="col-xl-3 col-md-6 mb-4">
                                        <div className="card border-left-primary shadow h-100 py-2">
                                            <div className="card-body">
                                                <div className="row no-gutters align-items-center">
                                                    <div className="col mr-2">
                                                        <div className="text-xs font-weight-bold text-primary text-uppercase mb-1">Generowanie CV</div>
                                                        <div className="h5 mb-0 font-weight-bold text-gray-800">Do 12.12.2019</div>
                                                    </div>
                                                    <div className="col-auto">
                                                        <i className="fas fa-calendar fa-2x text-gray-300"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <button className="btn btn-primary btn-raised">KUP SUBSKRYPCJĘ</button>
                                    </div>
                                </div>
                            </div>
                        </div>}
                </div>
            </div>
        )
    }
}

export default withRouter(Profile);