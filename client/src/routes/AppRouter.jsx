import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Header from '../components/header/Header';
import Home from '../components/home/Home';
import Login from '../components/login/Login';
import Register from '../components/register/Register';
import Profile from '../components/profile/Profile';

const AppRouter = () => (
    <BrowserRouter>
        <div>
            <Header />
            <Switch>
                <Route path="/" component={Home} exact={true} />
                <Route path="/login" component={Login} />
                <Route path="/profil" component={Profile} />
                <Route path="/register" component={Register} />
            </Switch>
        </div>
    </BrowserRouter>
);

export default AppRouter;