module.exports = ({ skills, hobbys, experience, education, languages }) => {
    const today = new Date();
    const renderLanguageStars = (level) => {
        let template = ''
        for (let index = 0; index < level; index++) {
            template += '<i class="material-icons">star</i>';
        }
        for (let index = 0; index < 5 - level; index++) {
            template += '<i class="material-icons star-to-achieve">star</i>';
        }
        return template;
    };

    return `<!DOCTYPE html>
    <head>
        <title></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,500,700,800&display=swap" rel="stylesheet">
        <style>
            html,
            body {
                height: 100%;
            }
    
            #wrapper {
                background-image: url('https://cv-generator.s3.eu-central-1.amazonaws.com/basicTemplate.png');
                background-size: contain;
                background-repeat: no-repeat;
                min-height: 100%;
                padding-bottom: 40px;
            }
    
            .no-padding {
                padding: 0px;
            }
    
            .font-primary {
                font-family: 'Roboto', sans-serif;
            }
    
            h1,
            h2,
            h3,
            h4,
            h5,
            h6 {
                font-family: 'Roboto', sans-serif !important;
    
            }
    
            body {
                font-family: 'Roboto', sans-serif;
            }
    
            .text-white {
                color: white;
            }
    
            .section-header {
                font-weight: 500;
            }
    
            .font-light {
                font-weight: 300;
            }
    
            .progress {
                background-color: rgba(245, 245, 245, 0.3);
                border-radius: 20px;
                height: 10px;
            }
    
            .progress-bar {
                background-color: white;
            }

            .item-list {
                list-style: none;
                padding: 0;
            }
    
            .item-list-2 {
                padding-left: 16px;
            }
    
            .item-list li,
            .item-list-2 li {
                margin-top: 16px;
            }
    
            .mt-1 {
                padding-top: 20px;
            }
    
            .right-column-template {
                padding-left: 100px;
            }
    
            .color-primary {
                color: #6a497e;
            }
    
            .subtitle {
                font-weight: 500;
            }

            .star-to-achieve {
                color: #ccc;
            }
            
            .pl-0 {
                padding-left:0;
            }
        </style>
    </head>
    
    <body>
        <div id="wrapper" class="container container-fluid" style="padding: 20px;">
            <div id="content">
                <div class="col-xs-12">
                    <div class="row" style="padding: 15px;">
                        <div class="col-xs-12 d-flex" style="margin-left: 92px; display: flex; align-items: center;">
                            <img src="./images/avatar.png" alt="" style="height: 250px; width: 250px;">
                            <div style="margin-left: 20px">
                                <h1 class="font-primary" style="color: white; font-weight: 800; font-size: 36px!important">
                                    Anna Nowak</h1>
                                <h3 class="font-primary"
                                    style="color: white; font-weight: 300; font-size: 22px!important; margin-top: 0">Graphic
                                    Designer</h3>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 100px">
                        <div class="col-xs-3 no-padding">
                            <div class="col-xs-12 text-center text-white">
                                <div class="row no-padding">
                                    <h4 class="section-header">Dane osobowe</h4>
                                    <i class="material-icons mt-1">place</i>
                                    <p class="font-light">Miasto, ul. Przypadkowa 123</p>
                                </div>
                                <div class="row no-padding">
                                    <i class="material-icons mt-1">phone</i>
                                    <p class="font-light">Miasto, ul. Przypadkowa 123</p>
                                </div>
                                <div class="row no-padding">
                                    <i class="material-icons mt-1">email</i>
                                    <p class="font-light">Miasto, ul. Przypadkowa 123</p>
                                </div>
                            </div>
                            <div class="col-xs-12 text-white">
                                <div class="row no-padding">
                                    <h4 class="section-header mt-1">Umiejętności</h4>
                                </div>
                                ${
                                    skills.map(skill => (
                                    `
                                       <div class="row no-padding">
                                            <p class="font-light">${skill.name}</p>
                                            <div class="progress">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0"
                                                    aria-valuemax="100" style="width: ${skill.level * 20}%;">
                                                </div>
                                            </div>
                                        </div>
                                    `
                                    )).join('')
                                }
                            </div>
                            <div class="col-xs-12 text-white">
                                <div class="row no-padding">
                                    <h4 class="section-header mt-1">Zainteresowania</h4>
                                    <ul class="item-list">
                                        ${
                                            hobbys.map(hobby => `<li class="font-light">${hobby.name}</li> `).join('')
                                        }
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-9 no-padding right-column-template">
                            <div class="col-xs-12">
                                <div class="row no-padding">
                                    <h4 class="section-header color-primary">Doświadczenie</h4>
                                </div>
                                ${
                                    experience.map(exp => 
                                    `
                                    <div class="row no-padding">
                                        <h5 class="subtitle">${exp.name}</h5>
                                        <p>${exp.occupationFrom} - ${exp.occupationFrom} | ${exp.occupation}</p>
                                        <p class="font-light">${exp.description}</p>
                                    </div>
                                    `).join('')
                                }
                            </div>
                            <div class="col-xs-12">
                                <div class="row no-padding">
                                    <h4 class="section-header color-primary">Wykształcenie</h4>
                                </div>
                                ${
                                    education.map(ed => 
                                    `
                                    <div class="row no-padding">
                                        <h5 class="subtitle">${ed.schoolname}</h5>
                                        <p>${ed.educationFrom} - ${ed.educationTo} | ${ed.profesion} </p>
                                    </div>
                                    `).join('')
                                }
                            </div>
                            <div class="col-xs-12">
                                <div class="row no-padding">
                                    <h4 class="section-header color-primary">Języki</h4>
                                </div>
                                <div class="row no-padding">
                                    ${
                                        languages.map(lang => 
                                        `
                                        <div class="col-xs-4 pl-0">
                                            <h5 class="subtitle">${lang.name}</h5>
                                            ${renderLanguageStars(lang.level)}
                                        </div>
                                        `).join('')
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
    
    </html>
    `;
};
